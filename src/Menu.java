
import static java.util.Arrays.asList;



import java.awt.EventQueue;


import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;


import org.bson.Document;

import com.mongodb.Block;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.MongoClient;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
//import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.IndexOptions;
import com.mongodb.client.model.Indexes;


import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Locale;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

import java.lang.String;
import javax.swing.JLabel;




public class Menu extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8117343173451454890L;
	private JPanel contentPane;
	private JTextField textField;
	private JTextField textField_1;
	private MongoClient mongoClient;    
    private MongoDatabase mongodb; 
    public static String baseDatos;
    public static String collection;
    public static String datos;
    public static String datobynombre;
    public static String name;
    public static String lastName;
    public static String grado;
    public static String fecha;
    public static String Uni;
    public static String Asignatura1;
    public static String Asignatura2;
    public static String Asignatura3;
    public static String Asignatura4;
    public static String Asignatura5;
    public static String dni;
    public static String campo;
    public static int edad;
  
	/**
	 * Launch the application.
	 */
	static int option =0;
	public static void main(String[] args) {
		
			
		
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Menu frame = new Menu();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	
			
		 
	}

	/**
	 * Create the frame.
	 */
	
	public Menu() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 757, 813);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setVisible(false);
		panel.setBounds(0, 0, 739, 566);
		panel.setLayout(null);
		
		textField = new JTextField();
		textField.setBounds(110, 167, 509, 243);
		panel.add(textField);
		textField.setColumns(10);
		
		JTextPane textPane = new JTextPane();
		textPane.setBounds(240, 45, 249, 22);
		panel.add(textPane);
		
		JLabel lblNewLabel = new JLabel("Inserte el nombre de la base de datos");
		
		lblNewLabel.setBounds(239, 610, 227, 16);
		contentPane.add(lblNewLabel);
		lblNewLabel.setVisible(false);
		
		
		JLabel lblNewLabel_2 = new JLabel("Inserte el nombre de la colecci�n");
		lblNewLabel_2.setBounds(259, 610, 227, 16);
		contentPane.add(lblNewLabel_2);
		lblNewLabel_2.setVisible(false);
		
		JLabel lblNewLabel_1 = new JLabel("Inserta el nombre del alumno...(continua en la consola)");
		lblNewLabel_1.setBounds(194, 610, 371, 16);
		contentPane.add(lblNewLabel_1);
		lblNewLabel_1.setVisible(false);
		
		JLabel lblNewLabel_3 = new JLabel("Escriba el nombre del campo a buscar");
		lblNewLabel_3.setBounds(270, 610, 219, 16);
		contentPane.add(lblNewLabel_3);
		lblNewLabel_3.setVisible(false);
		
		JLabel lblNewLabel_4 = new JLabel("Escriba el nombre de la colecci�n que desea borrar");
		lblNewLabel_4.setBounds(227, 610, 301, 16);
		contentPane.add(lblNewLabel_4);
		lblNewLabel_4.setVisible(false);
		
		JLabel lblNewLabel_5 = new JLabel("Escriba el nombre de la base de datos que desea eliminar");
		lblNewLabel_5.setBounds(200, 610, 338, 16);
		contentPane.add(lblNewLabel_5);
		lblNewLabel_5.setVisible(false);
		
		JLabel lblNewLabel_6 = new JLabel("Pulse guardar para confirmar la acci�n");
		lblNewLabel_6.setBounds(239, 610, 338, 16);
		contentPane.add(lblNewLabel_6);
		lblNewLabel_6.setVisible(false);
	
		
		JButton btnNewButton_4 = new JButton("Guardar");
		btnNewButton_4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnNewButton_4.addMouseListener(new MouseAdapter() {
			@SuppressWarnings("deprecation")
			@Override
			public void mouseClicked(MouseEvent arg0) {
				
				
				
				if(option==1) { //Accede base de datos
					
					
					baseDatos=textField_1.getText();
					textField_1.setText(null);
					System.out.println("Base datos: "+baseDatos);
				
				}
				
				else if(option==2) { //Accede colecci�n
					
					
					collection=textField_1.getText();
					textField_1.setText(null);
					System.out.println("base de datos : "+baseDatos +" con collection: "+collection);
					
				       
				
				}
				
				else if(option==3) {   //Inserta dato
					datos=textField_1.getText();
					textField_1.setText(null);
					lblNewLabel_1.setVisible(true);
					@SuppressWarnings("resource")
					Scanner sc = new Scanner (System.in);

					System.out.println("Inserte la edad: ");
					edad=sc.nextInt();
					@SuppressWarnings("resource")
					Scanner in = new Scanner (System.in);
							
					System.out.println("Inserte el dni: ");
					dni=in.nextLine();
					
					
					System.out.println("Insertando el alumno: "+ datos+", en la coleci�n : "+collection +", de la base de datos:  "+baseDatos);
					
					 Menu javaMongodbInsertData = new Menu();
				      javaMongodbInsertData.connectDatabase();
				      javaMongodbInsertData.insertOneDataTest();      
				}
				
				else if(option==4) {  //Busca dato por nombre
					
						campo=textField_1.getText();
						textField_1.setText(null);
						System.out.println("Inserta el nombre a buscar del campo: "+ campo);
						@SuppressWarnings("resource")
						Scanner sc = new Scanner (System.in);
						datobynombre=sc.nextLine();
						
						Menu javaMongodbInsertData = new Menu();
					      javaMongodbInsertData.connectDatabase();
					      javaMongodbInsertData.listinventoryBynombre(datobynombre);
				}
				
				
				else if(option==5) { //Elimina colecci�n
					
					String Collectiondrop=textField_1.getText();
					textField_1.setText(null);
					
					MongoClient mongoClient = new MongoClient();
					DB db = mongoClient.getDB(baseDatos);
					
					if (db.collectionExists(Collectiondrop)) {
					    DBCollection myCollection = db.getCollection(Collectiondrop);
					    myCollection.drop();
					}
					else {
						System.out.println("No existe esa colecci�n en la base de datos: "+ baseDatos);
					}
					mongoClient.close();
				}
					
				else if(option==6) { //Elimina base de datos
						
					String databasedrop=textField_1.getText();
					textField_1.setText(null);
						
					MongoClient data = new MongoClient();
					MongoDatabase database =data.getDatabase(databasedrop);
					database.drop();
					data.close();
			
				}
			
				else if(option==7) { //Muestra todos los documentos de una colecci�n
					
					
					MongoClient mongoClient = new MongoClient();
					DB db = mongoClient.getDB(baseDatos);
					DBCollection collectionF = db.getCollection(collection);
					DBCursor cursor = collectionF.find();
				    while(cursor.hasNext())
				    {
				        System.out.println(cursor.next());
				    }
				    mongoClient.close();
				}
				
				
				else if(option==8) { //Inserta dato avanzado
					
					@SuppressWarnings("resource")
					Scanner sc = new Scanner (System.in);
					
					System.out.println("Inserte el nombre: ");
					name= sc.nextLine();
					
					System.out.println("Inserte los apellidos");
					lastName=sc.nextLine();
				
					System.out.println("Inserte el dni");
					dni=sc.nextLine();
					
					System.out.println("Inserte la universidad");
					Uni=sc.nextLine();
					
					System.out.println("Inserte el grado");
					grado=sc.nextLine();
					
					System.out.println("Fecha de comienzo de grado");
					fecha=sc.nextLine();
					
					System.out.println("Inserte la asignatura 1:");
					Asignatura1=sc.nextLine();
					
					System.out.println("Inserte la asignatura 2:");
					Asignatura2=sc.nextLine();
					
					System.out.println("Inserte la asignatura 3:");
					Asignatura3=sc.nextLine();
					
					System.out.println("Inserte la asignatura 4:");
					Asignatura4=sc.nextLine();
					
					System.out.println("Inserte la asignatura 5:");
					Asignatura5=sc.nextLine();

					Menu javaMongodbInsertData = new Menu();
				    javaMongodbInsertData.connectDatabase();
				    javaMongodbInsertData.insertArray();
				        
				}
				else if (option==9) {
					
					dni=textField_1.getText();
					textField_1.setText(null);
					@SuppressWarnings("resource")
					Scanner sc = new Scanner (System.in);
					System.out.println("Inserte el nombre: ");
					name= sc.nextLine();
					System.out.println("Inserte la edad: ");
					edad=sc.nextInt();
					
					
					@SuppressWarnings("resource")
					MongoClient mongoClient= new MongoClient();
					MongoDatabase db = mongoClient.getDatabase(baseDatos);
					MongoCollection<org.bson.Document> coll = db.getCollection(collection);

					Document doc1 = new Document("Dni",dni);
					Document doc2 = new Document("Nombre", name).append("Edad", edad) .append("Dni", dni);
					coll.findOneAndReplace(doc1, doc2);
					
				}
				
				
				else if(option==10) {
					
					dni=textField_1.getText();
					textField_1.setText(null);
					@SuppressWarnings("resource")
					MongoClient mongoClient= new MongoClient();
					MongoDatabase db = mongoClient.getDatabase(baseDatos);
					MongoCollection<org.bson.Document> coll = db.getCollection(collection);

					Document doc1 = new Document("Dni",dni);
					
					coll.findOneAndDelete(doc1);
					
				}
				
			
			}
		});
		btnNewButton_4.setBounds(304, 705, 97, 44);
		contentPane.add(btnNewButton_4);
		
	
		JButton btnNewButton = new JButton("Acceder/Crear base de datos");
		btnNewButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				option=1;
				lblNewLabel_3.setVisible(false);
				lblNewLabel_2.setVisible(false);
				lblNewLabel_1.setVisible(false);
				lblNewLabel_4.setVisible(false);
				lblNewLabel_6.setVisible(false);
				lblNewLabel.setText("Inserte el nombre de la base de datos");
				lblNewLabel.setVisible(true);
			}
		});
		
		
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
			}
		});
		btnNewButton.setBounds(12, 13, 715, 44);
		contentPane.add(btnNewButton);
		
		JButton btnNewButton_1 = new JButton("Acceder/Crear colecci�n");
		btnNewButton_1.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				option=2;
				lblNewLabel_3.setVisible(false);
				lblNewLabel.setVisible(false);
				lblNewLabel_1.setVisible(false);
				lblNewLabel_4.setVisible(false);
				lblNewLabel_6.setVisible(false);
				lblNewLabel_2.setVisible(true);
				
			}
		});
	
		btnNewButton_1.setBounds(12, 70, 715, 44);
		contentPane.add(btnNewButton_1);
		
		JButton btnNewButton_2 = new JButton("Buscar dato por campo");
		btnNewButton_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnNewButton_2.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				option=4;
				lblNewLabel.setVisible(false);
				lblNewLabel_2.setVisible(false);
				lblNewLabel_1.setVisible(false);
				lblNewLabel_4.setVisible(false);
				lblNewLabel_6.setVisible(false);
				lblNewLabel_5.setVisible(false);
				lblNewLabel_3.setVisible(true);
				
			}
		});
		btnNewButton_2.setBounds(12, 246, 715, 44);
		contentPane.add(btnNewButton_2);
		
		JButton btnNewButton_3 = new JButton("Insertar dato");
		btnNewButton_3.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				option=3;
				lblNewLabel_3.setVisible(false);
	 			lblNewLabel.setVisible(false);
				lblNewLabel_2.setVisible(false);
				lblNewLabel_4.setVisible(false);
				lblNewLabel_6.setVisible(false);
				lblNewLabel_1.setVisible(true);
			}
		});
		btnNewButton_3.setBounds(12, 127, 715, 44);
		contentPane.add(btnNewButton_3);
		
		textField_1 = new JTextField();
		textField_1.setBounds(12, 648, 715, 44);
		contentPane.add(textField_1);
		textField_1.setColumns(10);
		
		JButton btnNewButton_5 = new JButton("Borrar colecci�n");
		btnNewButton_5.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				option=5;
				lblNewLabel_3.setVisible(false);
	 			lblNewLabel.setVisible(false);
				lblNewLabel_2.setVisible(false);
				lblNewLabel_1.setVisible(false);
				lblNewLabel_5.setVisible(false);
				lblNewLabel_6.setVisible(false);
				lblNewLabel_4.setVisible(true);
				
				
			}
		});
		btnNewButton_5.setBounds(12, 476, 715, 44);
		contentPane.add(btnNewButton_5);
		
		JButton btnNewButton_6 = new JButton("Borrar base de datos");
		btnNewButton_6.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				
				option=6;
				lblNewLabel_3.setVisible(false);
	 			lblNewLabel.setVisible(false);
				lblNewLabel_2.setVisible(false);
				lblNewLabel_1.setVisible(false);
				lblNewLabel_4.setVisible(false);
				lblNewLabel_6.setVisible(false);
				lblNewLabel_5.setVisible(true);
				
			}
		});
		btnNewButton_6.setBounds(12, 533, 715, 44);
		contentPane.add(btnNewButton_6);
		
		JButton btnNewButton_7 = new JButton("Mostrar todo los documentos de una colecci�n");
		btnNewButton_7.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				
				option=7;
				lblNewLabel_3.setVisible(false);
	 			lblNewLabel.setVisible(false);
				lblNewLabel_2.setVisible(false);
				lblNewLabel_1.setVisible(false);
				lblNewLabel_4.setVisible(false);
				lblNewLabel_5.setVisible(false);
				lblNewLabel_6.setVisible(true);
				
			}
		});
		btnNewButton_7.setBounds(12, 189, 704, 44);
		contentPane.add(btnNewButton_7);
		
		JButton btnNewButton_8 = new JButton("Insertar dato (avanzado)");
		btnNewButton_8.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				
				option=8;
				lblNewLabel_3.setVisible(false);
				lblNewLabel.setVisible(false);
				lblNewLabel_2.setVisible(false);
				lblNewLabel_1.setVisible(false);
				lblNewLabel_4.setVisible(false);
				lblNewLabel_5.setVisible(false);
				lblNewLabel_6.setVisible(false);
				
			}
		});
		btnNewButton_8.setBounds(12, 418, 715, 45);
		contentPane.add(btnNewButton_8);
		
		JButton btnNewButton_9 = new JButton("Actualizar");
		btnNewButton_9.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				
				option=9;
				lblNewLabel_3.setVisible(false);
				lblNewLabel_2.setVisible(false);
				lblNewLabel_1.setVisible(false);
				lblNewLabel_4.setVisible(false);
				lblNewLabel_6.setVisible(false);
				lblNewLabel.setText("Inserte el dni del alumno a actualizar");
				lblNewLabel.setVisible(true);
			}
		});
		btnNewButton_9.setBounds(12, 303, 715, 45);
		contentPane.add(btnNewButton_9);
		
		JButton btnNewButton_10 = new JButton("Eliminar dato");
		btnNewButton_10.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				option=10;
				lblNewLabel_3.setVisible(false);
				lblNewLabel_2.setVisible(false);
				lblNewLabel_1.setVisible(false);
				lblNewLabel_4.setVisible(false);
				lblNewLabel_6.setVisible(false);
				lblNewLabel.setText("Inserte el dni del alumno a eliminar");
				lblNewLabel.setVisible(true);
			}
		});
		btnNewButton_10.setBounds(12, 361, 715, 44);
		contentPane.add(btnNewButton_10);
			
		
	}

    public void connectDatabase(){
        setMongoClient(new MongoClient());            
        setMongodb(getMongoClient().getDatabase(baseDatos));
    }

    public void insertOneDataTest(){
        try {            
        	
        	//Dni como unica clase(no puede haber mas de un dni con el mismo numero).
        	IndexOptions indexOptions = new IndexOptions().unique(true);
        	
            Document doc= new Document("Nombre",datos).append("Edad",edad).append("Dni", dni);
        	getMongodb().getCollection(collection).createIndex(Indexes.ascending("Dni"),indexOptions);
        	getMongodb().getCollection(collection).insertOne(doc);
        	
        }catch(Exception e) {
        	System.out.println(e.getCause());
        }
        	
           
        
    }
    public MongoClient getMongoClient() {
        return mongoClient;
    }
 
    public void setMongoClient(MongoClient mongoClient) {
        this.mongoClient = mongoClient;
    }
 
    public MongoDatabase getMongodb() {
        return mongodb;
    }
 
    public void setMongodb(MongoDatabase mongodb) {
        this.mongodb = mongodb;
    } 
    
    @SuppressWarnings("deprecation")
	public void listinventoryBynombre(String nombre){ 
        FindIterable<Document> iterable = getMongodb().getCollection(collection).find(new Document(campo, nombre));
        iterable.forEach(new Block<Document>() {
            @Override
            public void apply(final Document document) {
                System.out.println(document);
            }
        });
    } 
    public void insertArray(){ //insertar datos (avanzados).
        try {
            DateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'", Locale.ENGLISH);
            
           
            IndexOptions indexOptions = new IndexOptions().unique(true);
        	getMongodb().getCollection(collection).createIndex(Indexes.ascending("Dni"),indexOptions);
            getMongodb().getCollection(collection).insertOne(
                    new Document("address", asList(
                            new Document()
                                    .append("Nombre",name)
                                    .append("Apellidos",lastName)
                                    .append("Dni", dni)
                                    .append("Grado",grado)
                                    .append("FechaInicio", format.parse(fecha+"T00:00:00Z"))
                                    .append("Asignaturas", asList(Asignatura1,Asignatura2,Asignatura3,Asignatura4,Asignatura5))))
                            .append("Nombre", name)
                            .append("Apellidos", lastName)          
                            .append("Universidad", Uni));
            				
        } catch (ParseException ex) {
            Logger.getLogger(Menu.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}